<?php

include 'dictionary.php';

$appLang = '';

defineAppLang($appLang);

// Старт игры
do {
    // Генерируем колоду карт
    $actualCardList = generateCardDeck();

    // Получаем карты одним списком для упрощения работы
    $cardDeck = getCardList($actualCardList);

    // Вывод подсказки для завершения приложения
    showLogMessage(INPUT_EXIT_COMMAND_MESSAGE);

    do {
        // Вводим количество игроков через консоль
        $playersCount = inputInteger(INPUT_ENTER_PLAYERS_MESSAGE);

        // Вводим количество карт на одного игрока
        $cardByPlayer = inputInteger(INPUT_ENTER_CARDS_MESSAGE);

        // Подсчитываем, хватает ли карт на игру
        $startGameBoolean = checkCardShortage(count($cardDeck), $playersCount, $cardByPlayer);

        if ($startGameBoolean === false) {
            showError(INPUT_TOO_MANY_ERROR);
        }
    } while ($startGameBoolean === false);

    // Генерируем игроков
    $players = generatePlayers($playersCount);

    do {
        // Перемешиваем карты
        $cardDeck = shakeCardDeck($cardDeck);

        // Раздаем карты игрокам
        if (giveCardsToPlayers($cardByPlayer, $cardDeck, $players)) {
            // Получение статистики игроков и карт
            $rankTable = getRank($players, $actualCardList);

            // Вывод строк с игроками с списком карт и общим рангом карт
            echo generatePlayersDescription($actualCardList, $players, $rankTable);

            // Определение победителя(-ей)
            $players = getWinners($rankTable, $players);

            if (count($players) === 1) {
                $gameStops = true;
                showWinnerMessage($players, $rankTable);
            } else {
                $gameStops = false;
                $cardByPlayer = 1;
                showStartOverMessage($players);
            }
        } else {
            // Вывод сообщения о ничьей
            showLogMessage(INPUT_DRAW_MESSAGE);

            $gameStops = true;
        }
    } while ($gameStops === false);

    // Вывод сообщения об окончании партии
    showLogMessage(INPUT_LAP_IS_OVER_MESSAGE);

    $isStartOver = inputBoolean(INPUT_START_OVER_MESSAGE);
} while ($isStartOver === true);

showLogMessage(INPUT_GAME_OVER_MESSAGE);
// Конец игры

/**
 * Опрделение параметров языка на основе переданных аргументов
 *
 * @param string $appLang
 *
 * @return void
 */
function defineAppLang(&$appLang)
{
    global $argv;

    if (in_array($argv[1], AVAILABLE_DICTIONARIES, true)) {
        $appLang = $argv[1];
    } else {
        $appLang = DEFAULT_APP_LANG;
    }
}


/**
 * Генерация массива карт
 *
 * @return array
 */
function generateCardDeck()
{
    $deck = [];

    foreach (CARD_SUITS as $suit) {
        for ($rank = 2; $rank <= 14; $rank++) {
            switch ($rank) {
                case 11:
                case 12:
                case 13:
                case 14:
                    $cardName = getCardDeckDictionary('rank')[$rank] . ' ' . getCardDeckDictionary('suit')[$suit];
                    break;
                default:
                    $cardName = $rank . ' ' . getCardDeckDictionary('suit')[$suit];
            }

            $cardKey = "{$suit}_$rank";

            $deck[$cardKey]['name'] = $cardName;
            $deck[$cardKey]['rank'] = $rank;
        }
    }

    return $deck;
}

/**
 * Функция для перемешивания колодны карт
 *
 * @param array $cardDeck Колода карт
 *
 * @return array
 */
function shakeCardDeck($cardDeck)
{
    shuffle($cardDeck);

    return $cardDeck;
}

/**
 * Получить список доступных карт
 *
 * @param array $cardDeck Колода карт
 *
 * @return array
 */
function getCardList($cardDeck)
{
    return array_keys($cardDeck);
}

/**
 * Проверка, соответствует ли количесвто карт в колоде, на выдачу каждому игроку
 *
 * @param integer $cardDeckLength  Количество карт в колоде
 * @param integer $playersQuantity Количество игроков
 * @param integer $cardByPlayer    Количество карт на одного игрока
 *
 * @return bool
 */
function checkCardShortage($cardDeckLength, $playersQuantity,  $cardByPlayer)
{
    $maxCardForPlayers = $playersQuantity * $cardByPlayer;

    if ((int) $maxCardForPlayers > (int) $cardDeckLength) {
        $resultBoolean = false;
    } else {
        $resultBoolean = true;
    }

    return $resultBoolean;
}


/**
 * Генерация игроков
 *
 * @param integer $playersQuantity Количество игроков
 *
 * @return array
 */
function generatePlayers($playersQuantity)
{
    $players = [];

    $playersName = PLAYERS_NAMES;

    $countMaxPlayersNames = count($playersName);

    shuffle($playersName);

    for ($i = 0; $i < $playersQuantity; $i++) {
        if (empty($playersName)) {
            $players[$i]['name'] = $players[random_int(0, $countMaxPlayersNames)]['name'] . '_' . $i;
        } else {
            $players[$i]['name'] = array_shift($playersName);
        }

        // По идее игроки должны быть заложены в БД или как то еще и должны иметь свой собственный ID
        $players[$i]['id'] = $i + 1;
    }

    return $players;
}

/**
 * Раздача карт игрокам
 *
 * @param integer $cardByPlayer Количество карт на игрока
 * @param array   $cardDeck     Список карт
 * @param array   $players      Игроки
 *
 * @return bool
 */
function giveCardsToPlayers($cardByPlayer, &$cardDeck, &$players)
{
    if (checkCardShortage(count($cardDeck), count($players), $cardByPlayer)) {
        $isCardsExist = true;

        foreach ($players as &$player) {
            for ($i = 0; $i < $cardByPlayer; $i++) {
                $player['cards'][] = array_shift($cardDeck);
            }
        }
    } else {
        $isCardsExist = false;
    }

    return $isCardsExist;
}

/**
 * Подсчет ранга карт
 * Логика возвращаемого массива: ключ это ID игрока, а значение общий ранг карт игрока;
 * Пример чтения: игрок с ID {ключ} имеет общий ранг карт {значение}
 *
 * @param array $players        Игроки
 * @param array $actualCardList Свойства карт
 *
 * @return array
 */
function getRank($players, $actualCardList)
{
    $rankTable = [];

    foreach ($players as $player) {
        $rank = 0;

        foreach ($player['cards'] as $card) {
            $rank += $actualCardList[$card]['rank'];
        }

        $rankTable[$player['id']] = $rank;
    }

    return $rankTable;
}

/**
 * Генерация информация о игроках, их картах и ранге карт
 *
 * @param array $actualCardList Свойства карт
 * @param array $players        Игроки
 * @param array $rankTable      Таблица с общим счетом каждого игрока
 *
 * @return string
 */
function generatePlayersDescription($actualCardList, $players, $rankTable)
{
    $tableStrings = [];

    $countTableRows = count($players) + 1;

    $playersIds   = [getStatTableLabel('player_id')];
    $playersNames = [getStatTableLabel('name')];
    $cardsNames   = [getStatTableLabel('hand')];
    $cardsRank    = [getStatTableLabel('hand_rank')];

    $playersIdsLen   = mb_strlen($playersIds[0]);
    $playersNamesLen = mb_strlen($playersNames[0]);
    $cardsNamesLen   = mb_strlen($cardsNames[0]);
    $cardsRankLen    = mb_strlen($cardsRank[0]);

    foreach ($players as $player) {
        $playersIds[] = $playerId = $player['id'];
        $playersNames[] = $player['name'];
        $cardsRank[] = $rankTable[$playerId];

        $cards = [];

        foreach ($player['cards'] as $card) {
            $cards[] = $actualCardList[$card]['name'];
        }

        $cardsString = implode(', ', $cards);

        if (mb_strlen($cardsString) > $cardsNamesLen) {
            $cardsNamesLen = mb_strlen($cardsString);
        }

        $cardsNames[] = $cardsString;

        if (mb_strlen($playerId) > $playersIdsLen) {
            $playersIdsLen = mb_strlen($playerId);
        }

        if (mb_strlen($player['name']) > $playersNamesLen) {
            $playersNamesLen = mb_strlen($player['name']);
        }

        if (mb_strlen($rankTable[$playerId]) > $cardsRankLen) {
            $cardsRankLen = mb_strlen($rankTable[$playerId]);
        }
    }

    $glue = '|';

    for ($i = 0; $i < $countTableRows; $i++) {
        $string = $glue;
        $string .= ' ' . mbStrPad($playersIds[$i], $playersIdsLen, ' ') . ' ' . $glue;
        $string .= ' ' . mbStrPad($playersNames[$i], $playersNamesLen, ' ') . ' ' . $glue;
        $string .= ' ' . mbStrPad($cardsNames[$i], $cardsNamesLen, ' ') . ' ' . $glue;
        $string .= ' ' . mbStrPad($cardsRank[$i], $cardsRankLen, ' ') . ' ' . $glue;


        $tableStrings[] = $string;

        if ($i === 0) {
            $lineLen = mb_strlen($string);
            $lineString  = '';

            for ($s = 0; $s < $lineLen; $s++) {
                $lineString .= '-';
            }

            $tableStrings[] = $lineString;
        }
    }

    $statisticString = '';

    foreach ($tableStrings as $string) {
        $statisticString .= $string . PHP_EOL;
    }

    return PHP_EOL . $statisticString . PHP_EOL;
}

/**
 * Функция возвращающая актуальных (выигрывавших) игроков
 *
 * @param array $rankTable Таблица общим счетом каждого игрока
 * @param array $players   Игроки
 *
 * @return array
 */
function getWinners($rankTable, $players)
{
    $winnersWithRank = getWinnersByRank($rankTable);

    $actualPlayers = [];

    $playerIds = array_keys($winnersWithRank);

    foreach ($players as $player) {
        if (in_array($player['id'], $playerIds, true)) {
            $actualPlayers[] = $player;
        }
    }

    return $actualPlayers;
}

/**
 * Получаем игроков у которых максимальный ранг
 *
 * @param array $rankTable Таблица общим счетом каждого игрока
 *
 * @return array
 */
function getWinnersByRank($rankTable)
{
    arsort($rankTable);

    $winners = [];

    $tempMaxRank = 0;

    foreach ($rankTable as $playerId => $rank) {
        if ($rank >= $tempMaxRank) {
            $tempMaxRank = $rank;
            $winners[$playerId] = $rank;
        } else {
            break;
        }
    }

    return $winners;
}


/**
 * Функция для введения числовых значений
 *
 * @param integer $messageNumber Номер сообщения которое видет пользователь
 *
 * @return int
 */
function inputInteger($messageNumber)
{
    do {
        $inputBoolean = true;

        showLogMessage($messageNumber);
        $inputValue = readline();

        if (checkInputBoolean($inputValue) === false) {
            showLogMessage(INPUT_GAME_OVER_MESSAGE);
            exit();
        }

        if (!is_numeric($inputValue)) {
            $inputBoolean = false;
            showError(INPUT_INTEGER_ERROR);
        }

        if ($inputValue <= 0) {
            $inputBoolean = false;
            showError(INPUT_INTEGER_MUST_BE_ERROR);
        }

        $inputValue = (int) $inputValue;
    } while ($inputBoolean === false);

    return $inputValue;
}

/**
 * Проврка вводимого значения на булевый тип
 *
 * @param integer $messageNumber Номер сообщения которое выводится пользователю перед вводом
 *
 * @return bool
 */
function inputBoolean($messageNumber)
{
    $returnBoolean = false;

    do {
        showLogMessage($messageNumber);
        $inputValue = readline();
        $inputBoolean = checkInputBoolean($inputValue);

        if ($inputBoolean === null) {
            echo sprintf(getErrorMessage(INPUT_COMMAND_NOT_FOUND_ERROR), $inputValue);
            $inputBoolean = false;
        } else {
            switch ($inputBoolean) {
                case true:
                    $returnBoolean = $inputBoolean = true;
                    break;
                case false:
                    $inputBoolean = true;
                    $returnBoolean = false;
                    break;
                default:
                    $inputBoolean = true;
            }
        }
    } while ($inputBoolean === false);

    return $returnBoolean;
}

/**
 * Функция для проверки передоваемого значения для проверки да или нет
 *
 * @param $inputValue
 *
 * @return bool|null
 */
function checkInputBoolean($inputValue)
{
    $inputValue = mb_strtolower($inputValue);

    if (in_array($inputValue, STRINGS_FOR_YES_BOOL, true)) {
        $returnBoolean = true;
    } elseif (in_array($inputValue, STRINGS_FOR_NO_BOOL, true)) {
        $returnBoolean = false;
    } else {
        $returnBoolean = null;
    }

    return $returnBoolean;
}


/**
 * Функция для вывода сообщения об ошибки в лог
 *
 * @param integer $errorNumber Номер ошибки
 *
 * @return void
 */
function showError($errorNumber)
{
    echo 'Ошибка: ' . getErrorMessage($errorNumber);
}

/**
 * Функция для вывода сообщения в лог
 *
 * @param integer $messageNumber Номер сообщения
 *
 * @return void
 */
function showLogMessage($messageNumber)
{
    echo getLogMessage($messageNumber);
}

/**
 * Генерация сообщения с именами игроков, которые вынуждены тянуть еще одну карту
 *
 * @param $players
 *
 * @return void
 */
function showStartOverMessage($players)
{
    $playersNames = [];

    foreach ($players as $player) {
        $playersNames[] = $player['name'];
    }

    echo sprintf(getLogMessage(INPUT_GAMELOOP_MESSAGE), implode(', ', $playersNames));
}

/**
 * Генерация итогового сообщения с победителем
 *
 * @param array $players
 * @param array $rankTable
 *
 * @return void
 */
function showWinnerMessage($players, $rankTable)
{
    $player = array_shift($players);

    echo sprintf(getLogMessage(INPUT_VICTORY_MESSAGE), $player['name'], $rankTable[$player['id']]);
}


/**
 * Функция для возвражения массива с словарем для колоды карт
 *
 * @param string $key Ключ массива, который необходимо вернуть
 *
 * @return array
 */
function getCardDeckDictionary($key)
{
    global $appLang;

    return CARD_DECK_DICTIONARY[$appLang][$key];
}

/**
 * Функция для возвращения текста ошибки
 *
 * @param integer $errorNumber
 *
 * @return string
 */
function getErrorMessage($errorNumber)
{
    global $appLang;

    return ERROR_MESSAGES[$appLang][$errorNumber];
}

/**
 * Функция для возвращения сообщения в лог
 *
 * @param integer $messageNumber Номер сообщения
 *
 * @return string
 */
function getLogMessage($messageNumber)
{
    global $appLang;

    return LOG_MESSAGES[$appLang][$messageNumber];
}

/**
 * Вовращение текста лейбла для таблицы статистики игроков
 *
 * @param string $labelName Наименование лейбла
 *
 * @return string
 */
function getStatTableLabel($labelName)
{
    global $appLang;

    return STATISTIC_TABLE_LABELS[$appLang][$labelName];
}


/**
 * mb_str_pad imitate
 *
 * @param        $input
 * @param        $pad_length
 * @param string $pad_string
 * @param int    $pad_type
 * @param null   $encoding
 *
 * @return string
 * @author naitsirch - https://gist.github.com/nebiros/226350#gistcomment-580052
 *
 */
function mbStrPad($input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT, $encoding = null)
{
    if (!$encoding) {
        $diff = strlen($input) - mb_strlen($input);
    }
    else {
        $diff = strlen($input) - mb_strlen($input, $encoding);
    }
    return str_pad($input, $pad_length + $diff, $pad_string, $pad_type);
}
