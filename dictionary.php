<?php

// Константы для доступных языков приложения
define('LANG_RUS', 'rus');
define('LANG_ENG', 'eng');
define('LANG_SHORT', 'short');

// Язык приложения
define('DEFAULT_APP_LANG', LANG_RUS);

// Доступные языки приложения
define('AVAILABLE_DICTIONARIES', [LANG_RUS, LANG_ENG, LANG_SHORT]);

// Константы для номеров ошибок.
define('INPUT_INTEGER_ERROR', 0);
define('INPUT_TOO_MANY_ERROR', 1);
define('INPUT_INTEGER_MUST_BE_ERROR', 2);
define('INPUT_COMMAND_NOT_FOUND_ERROR', 3);

// Константы для номеров сообщений
define('INPUT_ENTER_PLAYERS_MESSAGE', 0);
define('INPUT_ENTER_CARDS_MESSAGE', 1);
define('INPUT_GAMELOOP_MESSAGE', 2);
define('INPUT_VICTORY_MESSAGE', 3);
define('INPUT_LAP_IS_OVER_MESSAGE', 4);
define('INPUT_DRAW_MESSAGE', 5);
define('INPUT_START_OVER_MESSAGE', 6);
define('INPUT_GAME_OVER_MESSAGE', 7);
define('INPUT_EXIT_COMMAND_MESSAGE', 8);

// Константы для мастей
define('CARD_SUITE_DIAMOND', 'diamond');
define('CARD_SUITE_CLUB', 'club');
define('CARD_SUITE_HEART', 'heart');
define('CARD_SUITE_SPADE', 'spade');

define('STRINGS_FOR_YES_BOOL', ['yes', 'y', 'ye', 'да', 'д']);
define('STRINGS_FOR_NO_BOOL', ['no', 'n', 'нет', 'не', 'н', 'выход', 'вых', 'exit']);


// Список ошибок
define('ERROR_MESSAGES', [
    LANG_RUS => [
        INPUT_INTEGER_ERROR           => 'Введенное значение должно быть целым числом!' . PHP_EOL,
        INPUT_TOO_MANY_ERROR          => 'Задано слишком большое количество игроков или карт. Пожалуйста, введите значения заново...' . PHP_EOL,
        INPUT_INTEGER_MUST_BE_ERROR   => 'Введенное число не может быть нулем или меньше!' . PHP_EOL,
        INPUT_COMMAND_NOT_FOUND_ERROR => 'Команда "%s" не найдена...' . PHP_EOL,
    ],
    LANG_ENG => [
        INPUT_INTEGER_ERROR           => 'The value entered must be an integer!' . PHP_EOL,
        INPUT_TOO_MANY_ERROR          => 'Too many players or cards are set. Please enter values again ...' . PHP_EOL,
        INPUT_INTEGER_MUST_BE_ERROR   => 'The number entered cannot be zero or less!' . PHP_EOL,
        INPUT_COMMAND_NOT_FOUND_ERROR => 'Command "%s" not found ...' . PHP_EOL,
    ],
]);

// Список сообщений
define('LOG_MESSAGES', [
    LANG_RUS => [
        INPUT_ENTER_PLAYERS_MESSAGE => 'Введите количество игроков:' . PHP_EOL,
        INPUT_ENTER_CARDS_MESSAGE   => 'Введите количество карт на одного игрока:' . PHP_EOL,
        INPUT_GAMELOOP_MESSAGE      => 'Игра принимает неожиданный поворот! Игроки %s вынуждены взять еще по одной карте, так как у них одинаковое количество очков!!!' . PHP_EOL,
        INPUT_VICTORY_MESSAGE       => 'ИГРОК %s ОДЕРЖИВАЕТ ПОБЕДУ С ЛИДИРУЮЩИМ ИТОГОВЫМ СЧЕТОМ %s!!!' . PHP_EOL,
        INPUT_LAP_IS_OVER_MESSAGE   => 'Партия окончена.' . PHP_EOL,
        INPUT_DRAW_MESSAGE          => 'На всех игроков не хватает карт - НИЧЬЯ' . PHP_EOL,
        INPUT_START_OVER_MESSAGE    => 'Хотите начать заново? (Д/Н)' . PHP_EOL,
        INPUT_GAME_OVER_MESSAGE     => 'Игра окончена.' . PHP_EOL,
        INPUT_EXIT_COMMAND_MESSAGE  => 'Для завершения игры введите команду "Выход".' . PHP_EOL,
    ],
    LANG_ENG => [
        INPUT_ENTER_PLAYERS_MESSAGE => 'Enter the number of players:' . PHP_EOL,
        INPUT_ENTER_CARDS_MESSAGE   => 'Enter the number of cards per player:' . PHP_EOL,
        INPUT_GAMELOOP_MESSAGE      => 'The game takes an unexpected turn! The players of %s are forced to take on one more card, since they have the same number of points!!!' . PHP_EOL,
        INPUT_VICTORY_MESSAGE       => 'PLAYER %s KEEP YOUR VICTORY WITH A LEADING FINAL ACCOUNT %s !!!' . PHP_EOL,
        INPUT_LAP_IS_OVER_MESSAGE   => 'The lap is over.' . PHP_EOL,
        INPUT_DRAW_MESSAGE          => 'There are not enough cards for all players - DRAW' . PHP_EOL,
        INPUT_START_OVER_MESSAGE    => 'Want to start over? (Y/N)' . PHP_EOL,
        INPUT_GAME_OVER_MESSAGE     => 'The game is over.' . PHP_EOL,
        INPUT_EXIT_COMMAND_MESSAGE  => 'To end the game, enter the "Exit" command.' . PHP_EOL,
    ],
]);

// Словарь для генерации колоды
define('CARD_DECK_DICTIONARY', [
    LANG_RUS   => [
        'suit' => [
            CARD_SUITE_DIAMOND => 'бубны',
            CARD_SUITE_CLUB    => 'пики',
            CARD_SUITE_HEART   => 'черви',
            CARD_SUITE_SPADE   => 'трефы',
        ],
        'rank' => [
            11 => 'валет',
            12 => 'дама',
            13 => 'король',
            14 => 'туз',
        ]
    ],
    LANG_ENG   => [
        'suit' => [
            CARD_SUITE_DIAMOND => 'diamond',
            CARD_SUITE_CLUB    => 'club',
            CARD_SUITE_HEART   => 'heart',
            CARD_SUITE_SPADE   => 'spade',
        ],
        'rank' => [
            11 => 'jack',
            12 => 'queen',
            13 => 'king',
            14 => 'ace',
        ]
    ],
    LANG_SHORT => [
        'suit' => [
            CARD_SUITE_DIAMOND => '♦',
            CARD_SUITE_CLUB    => '♣',
            CARD_SUITE_HEART   => '♥',
            CARD_SUITE_SPADE   => '♠',
        ],
        'rank' => [
            11 => 'J',
            12 => 'Q',
            13 => 'K',
            14 => 'A',
        ]
    ]
]);

// Словарь для генерации лейблов статистики игроков
define('STATISTIC_TABLE_LABELS', [
    LANG_RUS => [
        'player_id' => 'ИД игрока',
        'name'      => 'Имя игрока',
        'hand'      => 'Рука',
        'hand_rank' => 'Общий ранг карт',
    ],
    LANG_ENG => [
        'player_id' => 'Player ID',
        'name'      => 'Player name',
        'hand'      => 'Cards',
        'hand_rank' => 'Cards rank',
    ],
]);

// Массив с мастями карт
define('CARD_SUITS', [
    CARD_SUITE_DIAMOND, // бубны
    CARD_SUITE_CLUB,    // пики
    CARD_SUITE_HEART,   // черви
    CARD_SUITE_SPADE,   // трефы
]);

// Имена игроков
define('PLAYERS_NAMES', [
    'YourMommy',
    'Sergey Brin',
    'O4K03ABP',
    'Frost45000',
    'BAZiLLi0',
    'YokeLore',
    'will.i.am',
    'GoodPlayer',
    'alexey_89',
    'felix_mind',
    '9I_KPEBEDKO!',
]);